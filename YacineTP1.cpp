/*  INF3105 gr. 030 Été 2019                             *
 *  Structures de données et algorithmes                 *
 *  TP1                                                  *
 *                                                       *
 *  HASY04089702 Yassine Hasnaoui                        *
 *                                                       */

#include <iostream>
 #include <iomanip>
 #include <string>
 #include <fstream>
 #include <math.h>
 #include <vector>

using namespace std;

/** Cette classe Represente un Sommet de rectangle.
*/
class Sommet{
    public:
     Sommet(){}
    /** Constructeur qui cree un sommet avec les 
     * coordonnee x,y.
     * @param px position X du sommet.
     * @param py position Y du sommet.
    */
     Sommet(double px,double py){
         x=px;
         y=py;
     }
     double getX(){return x;}
     double getY(){return y;}

    protected:
    //Coordonnee (x,y) du sommet
    double x,y; 

};
/** Cette classe Represente une arete de rectangle.
*/
class Arete{
    public:
      Arete(){}
      //Dans le cas d'une arete bas/haut
    /** Constructeur qui cree une arete ( de type bas ou haut) avec  les positions
     * Xminimum et Xmaximum et sa position Y.
     * 
     * @param _type type de l'arete (bas ou haut).
     * @param x1 la position xmin.
     * @param x2 la position xmax.
     * @param numRec Numero permettant d'identifier a quel rectangle appartient l'arete
    */
      Arete(char _type,double x1, double x2, double pos_Y,int numRec){
          NumeroRect=numRec;
          posXmin=x1;
          posXmax=x2;
          posY=pos_Y;
          typeArete=_type;
      }

      //Dans le cas d'une arete gauche/droite
    /** Constructeur qui cree une arete ( de type gauche ou droite) avec  les positions
     * Yminimum et Ymaximum et sa position X.
     * @param _type type de l'arete (bas ou haut).
     * @param y1 la position ymin.
     * @param y2 la position ymax.
     * @param numRec Numero permettant d'identifier a quel rectangle appartient l'arete
     * @param signature signature du constructeur pour les aretes gauche/droite
    */
      Arete(char _type,double y1, double y2,double pos_X,int numRec, int signature){
          NumeroRect=numRec;
          posYmin=y1;
          posYmax=y2;
          posX=pos_X;
          typeArete=_type;

      }
      //getters
      double getPosXmin(){return posXmin;}
      double getPosXmax(){return posXmax;}
      double getPosYmin(){return posYmin;}
      double getPosYmax(){return posYmax;}
      double getPosX(){return posX;}
      double getPosY(){return posY;}
    protected:
    //Numero permettant d'identifier a quel rectangle appartient l'arete
    int NumeroRect;
    // type d'arete : bas/haut/gauche/droite
    char typeArete;
    double posXmin,posXmax,posYmin,posYmax,posX,posY;
    
};
/** Cette classe Represente un Rectangle.
*/
class Rectangle { 
  
    public:
    /** Constructeur qui cree un rectangle ayant un type 
     * positif ou negatif,un numeros de rectangle, les 
     * coordonnees X,Y du pointmilieux ainsi que qu'une hauteur 
     * et une largeur.
     * @param TypeRectangle le type du rectangle(positif ou negatif).
     * @param positionX positionX du point milieu.
     * @param positionY positionY du point milieu.
     * @param h hauteur du rectangle.
     * @param l largeur du rectangle.
     * @param Num Numeros du rectangle.
    */
    Rectangle (char TypeRectangle='x', double positionX=0, double positionY=0, double h=0, double l=0,int Num=0) { 
        //les coordonnee x,y peuvent etre negatives
         x=positionX;
         y=positionY; 
         m_hauteur=h; 
         m_largeur=l; 
         type=TypeRectangle;
         Aire=m_hauteur*m_largeur;
         numeros=Num;
         }
     //getters
     double aire() { return m_hauteur*m_largeur;}
     double perimetre() {return (2*(m_hauteur+m_largeur));}
     //top-right  
     Sommet getSommetMax(){ return Sommet (x+m_largeur/2,y+m_hauteur/2);}
     //Bottom-left 
     Sommet getSommetMin(){ return Sommet (x-m_largeur/2,y-m_hauteur/2);} 
     Arete getAreteBas(){return Arete('b',x-m_largeur/2,x+m_largeur/2,y-m_hauteur/2,numeros);}
     Arete getAreteHaut(){return Arete('h',x-m_largeur/2,x+m_largeur/2,y+m_hauteur/2,numeros);}
     Arete getAreteGauche(){return Arete('g',y-m_hauteur/2,y+m_hauteur/2,x-m_largeur/2,numeros,1);}
     Arete getAreteDroite(){return Arete('d',y-m_hauteur/2,y+m_hauteur/2,x+m_largeur/2,numeros,1);}
     char getType(){return type;}
     double getPositionX(){return x;}
     double getPositionY(){return y;}
     double getHauteur(){return m_hauteur;}
     double getLargeur(){ return m_largeur;}
     int getNumero(){return numeros;}
     //setters
     void setType(char rtype){type=rtype;}
     void setPositionX( double px){x=px;}
     void setPositionY(double py){y=py;}
     void setHauteur(double h){m_hauteur=h;}
     void setLargeur(double l){m_largeur=l;}
     void setNumero(int num){numeros=num;}


     protected:
     double x, y, m_hauteur, m_largeur,Aire;
     int numeros;
     // les 4 aretes du rectangle
     Arete areteBas,areteHaut,areteGauche,areteDroite;
     char type; 
};
/**
    Affiche les l'aire et le perimetre total de la forme 
    resultante.
    @param Airetotal Aire de la forme finale.
    @param perimetretotal Perimetre de la forme finale.
*/
void afficherResultats(double Airetotal,double perimetretotal) {
 cout<<"aire : "<<Airetotal<< endl;
 cout<<"perimetre : "<<perimetretotal<< endl;
}
/**
    Obtenir l'aire de lintersection de deux rectangles.
    @param r1 Le premier rectangle.
    @param r2 Le deuxième rectangle.
    @return La valeur de l'aire en†re les deux rectangles.
*/
double AireIntersection( Rectangle r1, Rectangle r2){
    double AI;
    double longeur=min(r1.getSommetMax().getX(),r2.getSommetMax().getX())-max(r1.getSommetMin().getX(),r2.getSommetMin().getX());
    double hauteur=min(r1.getSommetMax().getY(),r2.getSommetMax().getY())-max(r1.getSommetMin().getY(),r2.getSommetMin().getY());
    AI=longeur*hauteur;
 return AI;
}
/**
    Obtenir le rectangle issue de l'intersection
    entre deux rectangles
    .
    @param r1 Le premier rectangle.
    @param r2 Le deuxième rectangle.
    @return Le rectangle issue de l'intersection.
*/
Rectangle RectangleIntersection( Rectangle r1, Rectangle r2){
      Rectangle RecI;
      double largeur=min(r1.getSommetMax().getX(),r2.getSommetMax().getX())-max(r1.getSommetMin().getX(),r2.getSommetMin().getX());
      double hauteur=min(r1.getSommetMax().getY(),r2.getSommetMax().getY())-max(r1.getSommetMin().getY(),r2.getSommetMin().getY());
    
        RecI.setLargeur(largeur);
        RecI.setHauteur(hauteur);
        RecI.setPositionX(max(r1.getSommetMin().getX(),r2.getSommetMin().getX()) + (RecI.getLargeur()/2));
        RecI.setPositionY(max(r1.getSommetMin().getY(),r2.getSommetMin().getY())+ (RecI.getHauteur()/2));
        RecI.setType('I');
        // le recI prend le numero du rectangle avec qui il se croise
        RecI.setNumero(r2.getNumero());

        //si les rectangle se superposent on retourne r1
        if ( (r1.getSommetMax().getX()==r2.getSommetMax().getX()) && (r1.getSommetMax().getY()==r2.getSommetMax().getY()) 
        &&(r1.getSommetMin().getX()==r2.getSommetMin().getX()) && (r1.getSommetMin().getY()==r2.getSommetMin().getY())){
            RecI=r1;
        }
        return RecI;
}
/**
    Obtenir le perimetre issue des aretes de l'intersection
    entre deux rectangles.
    @param r1 Le premier rectangle.
    @param r2 Le deuxième rectangle.
    @return La valeur du perimetre .
*/
double PerimetreIntersection(Rectangle r1, Rectangle r2){
 return fabs(2*( ( min(r1.getSommetMin().getX(),r2.getSommetMin().getX()) - max(r1.getSommetMin().getX(),r2.getSommetMin().getX()) )
     + ( min(r1.getSommetMin().getY(),r2.getSommetMin().getY()) - max(r1.getSommetMin().getY(),r2.getSommetMin().getY())   ) ));
}
/**
    Verifie s'il y a une intersection entre deux rectangles.
    @param r1 Le premier rectangle.
    @param r2 Le deuxième rectangle.
    @return Vraie s'il y a une intersection, faux sinon.
*/
bool Intersection( Rectangle r1, Rectangle r2){
    bool Intersec=false;
    double longeur=min(r1.getSommetMax().getX(),r2.getSommetMax().getX())-max(r1.getSommetMin().getX(),r2.getSommetMin().getX());
    double hauteur=min(r1.getSommetMax().getY(),r2.getSommetMax().getY())-max(r1.getSommetMin().getY(),r2.getSommetMin().getY());

    //si la longeur est negative, alors il n'y a pas intersection
    if (longeur>0 && hauteur>0 )
    {
        Intersec=true;
    }
 return Intersec;
}
/**
    Verifie si deux rectangles sont parfaitement identiques et si
    ils se superposent.
    @param r1 Le premier rectangle.
    @param r2 Le deuxième rectangle.
    @return Vraie si c'est le cas, faux sinon.
*/
bool equals(Rectangle r1, Rectangle r2){
    bool equals=false;
     if ( (r1.getSommetMax().getX()==r2.getSommetMax().getX()) && (r1.getSommetMax().getY()==r2.getSommetMax().getY()) 
        &&(r1.getSommetMin().getX()==r2.getSommetMin().getX()) && (r1.getSommetMin().getY()==r2.getSommetMin().getY())){ equals=true;}
            return equals;
}
/**
    Verifie si le nom du fichier entree existe et si il est de forme .txt.
    S'il n'existe pas, demander un nom valide.
    @param nom le nom du fichier
    @return le nom d'un fichier valide.
*/
string validerNomFichier(string nom){ 
    bool valide=false;
    string nomFichier=nom;
    //tant que ce n'est pas un fichier .txt
    while (nomFichier.find(".txt")==string::npos)
    {
        cout <<"Impossible d'ouvrir le fichier, veuillez entree un nom de fichier (.txt) valide. "<<endl;
        cin>> nomFichier;
    }
    ifstream input;
    input.open(nomFichier);

    while(input.fail()){
        
            cout <<"Impossible d'ouvrir le fichier, veuillez entree un nom de fichier (.txt) valide. "<<endl;
            cin>> nomFichier;
            input.open(nomFichier);
      
    }
    input.close();
    input.clear();

    return nomFichier;
}
/**
    Obtenir le perimetre de trops des arete a ete compté lors de l'intersection de deux rectangles .
    @param r1 Le premier rectangle.
    @param r2 Le deuxième rectangle.
    @return La valeur du perimetre a enlever.
*/
double PerimetreAreteIntersectee(Rectangle r1, Rectangle r2){
    double perimetre_a_enlever=0;
    double hauteur=min(r1.getSommetMax().getY(),r2.getSommetMax().getY())-max(r1.getSommetMin().getY(),r2.getSommetMin().getY());
    double largeur=min(r1.getSommetMax().getX(),r2.getSommetMax().getX())-max(r1.getSommetMin().getX(),r2.getSommetMin().getX());

    Rectangle rI=RectangleIntersection(r1,r2);
    //pour arete bas/haut
    if (r1.getAreteBas().getPosY()==r2.getAreteBas().getPosY())
    {
         if (Intersection(r1,  r2))
         {
             perimetre_a_enlever=perimetre_a_enlever+rI.getLargeur();
         }
         
         
    }

       if (r1.getAreteHaut().getPosY()==r2.getAreteHaut().getPosY())
    {
         if (Intersection(r1,  r2))
         {
             perimetre_a_enlever=perimetre_a_enlever+rI.getLargeur();
         }
         
    }
    //pour arete gauche/droite
     if (r1.getAreteGauche().getPosX()==r2.getAreteGauche().getPosX())
   {
         if (Intersection(r1,  r2))
         {
             perimetre_a_enlever=perimetre_a_enlever+rI.getHauteur();
         }
         
    }
         
         if (r1.getAreteDroite().getPosX()==r2.getAreteDroite().getPosX())
   {
         if (Intersection(r1,  r2))
         {
             perimetre_a_enlever=perimetre_a_enlever+rI.getHauteur();

         } 
         
         
    }

    if (rI.getLargeur()==0)
    {
        perimetre_a_enlever=perimetre_a_enlever+rI.getHauteur();

    }else if (rI.getHauteur()==0)
    {
                perimetre_a_enlever=perimetre_a_enlever+rI.getLargeur();

    }
    
    

    
 return perimetre_a_enlever;
}
/* *
    Verifie s'il y a trois aretes en intersection avec un autre
    rectangles pour savoir si le perimetre de la forme dois etre modifier

    @param r1 Le premier rectangle.
    @param r2 Le deuxième rectangle.
    @return Vraie s'il y a trois aretes en intersection .
*/
bool VerifierNbAreteIntersection(Rectangle r1, Rectangle r2){ //recT,recP
    bool boolean=false;
    int compteur=0;

    //Comparaison des arete Bas/haut
    if(r1.getAreteBas().getPosY()!=r2.getAreteBas().getPosY())
    {
        compteur++;
    }
    if(r1.getAreteHaut().getPosY()!=r2.getAreteHaut().getPosY())
    {
        compteur++;
    }
    //Comparaison des arete Bas/haut
    if(r1.getAreteGauche().getPosX()!=r2.getAreteGauche().getPosX())
    {
        compteur++;
    }
    if(r1.getAreteDroite().getPosX()!=r2.getAreteDroite().getPosX())
    {
        compteur++;
    }
    
    //si le nb darete negative a linterieur d'un rectangle positif >2, alors vraie
    if (compteur==3)
    {
        boolean=true;
    }
    return boolean;   
}
/**
    Lecture des rectangles contenue dans le fichier.
    Place les rectangle dans Vectors et incremente 
    le nombre de rectangles lue.

    @ param RectanglesVectors Tableau de rectangle.
    @param nbRectangle le nombre de rectangles.
    @param ValeursInvalide booleen verifiant si les valeurs sont valides.
*/
void LectureFichier(vector<Rectangle> &RectanglesVectors, int& nbRectangle, bool& ValeursInvalide  ){
    double positionX,positionY,longueur,hauteur;
    char type; 


     string nomFichier;
    cout <<"Entrez le nom du fichier : ";
    cin>> nomFichier;
    nomFichier=validerNomFichier(nomFichier);

    ifstream reader (nomFichier);
    while(reader>> type>>positionX>>positionY>>longueur>>hauteur){
     Rectangle RectangleCourant(type,positionX,positionY,longueur,hauteur,nbRectangle);
     //ajout dans le tableau
     RectanglesVectors.push_back(RectangleCourant);
     nbRectangle++;
    }
    //si les valeurs sont invalide on sort du programme
    for (int i = 0; i <nbRectangle ; i++)
    {
        if(RectanglesVectors[i].getHauteur()<0 ||RectanglesVectors[i].getLargeur()<0)
        {
            ValeursInvalide=true;
        }
        
    }
}
/**
    place les rectangles positifs et negatifs dans un tableau vector separé.
    @param RectanglesVectors le tableau de rectangle.
    @param RecPositifsVectors tableau de rectangle positifs a remplir.
    @param RecNegatifsVectors tableau de rectangle negatifs a remplir.
    @param nbRectangle le nombre de rectangles.
    @param compteuPositif compteur de rectangles positifs.
    @param compteurNegatif compteur de rectangles negatifs
    */
void placerRectPositifsEtNegatifs(vector<Rectangle> &RectanglesVectors,vector<Rectangle> & RecPositifsVectors,vector<Rectangle> & RecNegatifsVectors,int &nbRectangle,int&compteuPositif,int & compteurNegatif){
        //placer les rectangle positifs & negatifs
   for (int i = 0; i < nbRectangle; i++){
        if (RectanglesVectors[i].getType()=='p')
        {
            RecPositifsVectors.push_back(RectanglesVectors[i]);
            compteuPositif++;
        }else if(RectanglesVectors[i].getType()=='n')
        {
                        RecNegatifsVectors.push_back(RectanglesVectors[i]);
                        compteurNegatif++;
        }
         
    }
}
/**
    Calcule et modifie l'aire et le perimetere des rectangles positifs contenu 
    dans le tableau de rectangles positifs.
    @param RecPositifsVectors tableau de rectangle positifs a remplir.
    @param AireTotal Aire total de la forme.
    @param PerimetreToral perimetre total de la forme.
    @param compteuPositif compteur du nb de rectangles positifs

*/
void CalculAireEtPerimetrePositif(vector<Rectangle> & RecPositifsVectors,double& AireTotal,double & PerimetreToral,int &compteuPositif){
    //Calcul de l'aire&perimetre totale positif
 for (int i = 0; i < compteuPositif; i++){         
          
          AireTotal = AireTotal+ RecPositifsVectors[i].aire();
          PerimetreToral=PerimetreToral+ RecPositifsVectors[i].perimetre();
          for (int j = i+1; j < compteuPositif; j++) { 
              //On ne copte pas les rectangles qui se superposent completement
              if (equals(RecPositifsVectors[i],RecPositifsVectors[j])){
              i++;
              }
          }
    
   }


 for (int i = 0; i < compteuPositif; i++)
 {
     for (int j = i+1; j < compteuPositif; j++)
     {
          // si des aretes se superposent
         if (equals(RecPositifsVectors[i],RecPositifsVectors[j])){
              i++;
        }else{
                PerimetreToral=PerimetreToral- PerimetreAreteIntersectee(RecPositifsVectors[i],RecPositifsVectors[j]);
               }
     } 
     
 }
}
/**
    Place les intersection entre les rectangles dans un tableau appart.
    Accumule l'aire des intersections dans une variable.
    soustrais le perimetre des intersections.

    @param RecPositifsVectors le tableau contenant les rectangles positifs.
    @param RecIntersecVectors le tableau d'intersections a remplir.
    @param AireI l'aire des intersection.
    @param PerimetreToral Le perimetre total de la forme.
    @param compteuPositif compteur des rect positifs.
    @param compteurIntersections compteur du nombre d'intersection

*/
void PlacerIntersectionDansTab(vector<Rectangle> & RecPositifsVectors,vector<Rectangle> & RecIntersecVectors ,double & AireI, double & PerimetreToral, int &compteuPositif,int& compteurIntersections){
    //On trouve les intersections et les places dans un tableau
 for (int i = 0; i < compteuPositif; i++)
 {
    //S'il y a intersection
    for (int j = i+1; j < compteuPositif; j++)
    {
           if(equals(RecPositifsVectors[i],RecPositifsVectors[j])) 
           { 
             //si les rectangles se superposent completement on ne fais rien 
           }else if (Intersection(RecPositifsVectors[i],RecPositifsVectors[j])){
               //s'il y a intersection partielle, on  l'Ajoute a l'aire Intersection
            Rectangle rI=RectangleIntersection(RecPositifsVectors[i],RecPositifsVectors[j]);
            rI.setNumero(RecPositifsVectors[j].getNumero());
            AireI=AireI+rI.aire();
            //On soustrait le perimetre de lintersection
            PerimetreToral=PerimetreToral-rI.perimetre();
            RecIntersecVectors.push_back(rI);
            compteurIntersections++;
        }
    }
 }
}
/**
    Detecte et calculs l'aire et le perimetre comptee de trops 
    a enlever lors des intersections.

    @param RecIntersecVectors le tableau d'intersections
    @param AireI l'aire des intersection.
    @param PerimetreToral Le perimetre total de la forme.
    @param AireTotal Aire total de la forme.
    @param compteurIntersections compteur du nombre d'intersection
   
*/
void CalculsAireEtPerimIntersection(vector<Rectangle> & RecIntersecVectors,double & AireI, double & PerimetreToral,double & AireTotal,int& compteurIntersections){
     //si le rect intersection est a linterieur dun rectangle positif, on le soustrai
  for (int i = 0; i < compteurIntersections; i++)
  {
      //si les rectanglesI se croisent et appartiennent au meme rectangle positif d'origine
      for (int j = i+1; j < compteurIntersections; j++)
      {
          if(Intersection(RecIntersecVectors[i],RecIntersecVectors[j]) &&(RecIntersecVectors[i].getNumero()==RecIntersecVectors[j].getNumero()))
            { 
              
                     AireI=AireI-AireIntersection(RecIntersecVectors[i],RecIntersecVectors[j]); 
                    
             }
                   if(Intersection(RecIntersecVectors[i],RecIntersecVectors[j]) )
            { 
              
                     Rectangle rI= RectangleIntersection(RecIntersecVectors[i],RecIntersecVectors[j]);
                     double perimetreEnlevee=PerimetreAreteIntersectee(RecIntersecVectors[i],RecIntersecVectors[j]);
                     PerimetreToral=PerimetreToral+ perimetreEnlevee;    
                 
             }
      }
  }
 AireTotal=AireTotal-AireI;
}
/**
    On soustrais les aires negatives de l'aire total de la forme positive.
    @param RecIntersecVectors le tableau d'intersections
    @param RecNegatifsVectors le tableau de rectangles negatifs.
    @param RecPositifsVectors le tableau de rectangles positifs.
    @param AireTotal Aire total de la forme.
    @param compteurIntersections compteur du nombre d'intersection.
    @param compteuPositif compteur du nb de rec positifs
    @param compteurNegatif compteur du nb de rec negatifs.
*/
void SoustractionAireNegative(vector<Rectangle> & RecIntersecVectors,vector<Rectangle> & RecNegatifsVectors,vector<Rectangle> & RecPositifsVectors,double & AireTotal,int& compteurIntersections,int&compteuPositif,int & compteurNegatif){
        compteurIntersections=0;
        // on vide le tableau des intersections positives
        RecIntersecVectors.clear();
 //Soustraire l'aire des rectangles negatifs
 for (int i = 0; i < compteurNegatif; i++)
 {

// on compare le rectangle negatif i aux rectangles positifs
    for (int j = 0; j < compteuPositif; j++)
    {
        if (equals(RecNegatifsVectors[i],RecPositifsVectors[j])) 
        {
            //Si le rec negatif se superpose completement au rec positif
            AireTotal=AireTotal-RecNegatifsVectors[i].aire();
        }else if(Intersection(RecNegatifsVectors[i],RecPositifsVectors[j]))
        {
            Rectangle rI=RectangleIntersection(RecNegatifsVectors[i],RecPositifsVectors[j]);
            RecIntersecVectors.push_back(rI);
            compteurIntersections++;
            AireTotal=AireTotal-rI.aire();
        }   
        
    }
 }

}
/**
    Copie le tableau d'intersection dans un autre tableau sans les doublons.
    @param RecIntersecVectors le tableau d'intersections
    @param compteurIntersections compteur du nombre d'intersection.
    @param RecIntersecVectorsSansDoublons le tableau a remplir.
*/
void RectangleIntersectionSansDoublons(vector<Rectangle> & RecIntersecVectors, int &compteurIntersections,vector<Rectangle> &RecIntersecVectorsSansDoublons){
    //Rectnagle Intersection sans les doublons
 for (int i = 0; i < compteurIntersections; i++)
 {
    bool add=true;
  for (int j = i+1; j < compteurIntersections; j++)
  {
       if (equals(RecIntersecVectors[i],RecIntersecVectors[j]))
       {
           add=false;
       }      
  }
  if(add==true){
      RecIntersecVectorsSansDoublons.push_back(RecIntersecVectors[i]);
     }
 }
}
/**
   Ajoute le perimetre issue d'un rectangle negatif qui est en intersections
   avec un rectangle positif.
   @param RecIntersecVectorsSansDoublons le tableau d'intersection sans les doublons
   @param RecNegatifsVectors le tableau de rectangles negatifs.
   @param RecPositifsVectors le tableau de rectangles positifs.
   @param compteurIntersections compteur du nombre d'intersection.
   @param PerimetreToral Le perimetre total de la forme.
   @param compteuPositif compteur du nb de rec positifs
   @param compteurNegatif compteur du nb de rec negatifs.
*/
void CalculsPerimNegatif(vector<Rectangle> & RecIntersecVectorsSansDoublons,vector<Rectangle> & RecNegatifsVectors,vector<Rectangle> & RecPositifsVectors,int& compteurIntersections,double &PerimetreToral,int&compteuPositif,int & compteurNegatif){
 //si un rectangke negatif est completement intersecté avec une intersection positive, on additonne
 //sont perimetre
 for (int i = 0; i < compteurNegatif; i++)
 {
      for (int j = 0; j < compteurIntersections; j++)
      {
            if (equals(RecNegatifsVectors[i],RecIntersecVectorsSansDoublons[j])){
                            PerimetreToral=PerimetreToral+RecNegatifsVectors[i].perimetre();
            }
      }
      
 }


 //s'il y a plus de 3 arete du rec negatif qui est en intersection avec un rec positif,on
 //ajoute (2* le minimum entre la hauteur et la largeur du rectangle intersectee)

 for (int i = 0; i < compteurNegatif; i++)
 {
     for (int j = 0; j < compteuPositif; j++)
     {
         //sil existe une intersection
       if(Intersection(RecNegatifsVectors[i],RecPositifsVectors[j]))
              {
             Rectangle rI=RectangleIntersection(RecNegatifsVectors[i],RecPositifsVectors[j]);
             rI.setNumero(RecNegatifsVectors[i].getNumero());
             //si rI n'est pas en intersection avec d'autre rectangles negatifs
             bool IntersectionRI=false;
              for (int z = 0; z < compteurNegatif; z++)
              {
                  if(Intersection(rI,RecNegatifsVectors[z])   && (rI.getNumero()!=RecNegatifsVectors[z].getNumero()))
                  {
                      IntersectionRI=true;
                  }
                  
              }
              
              if(IntersectionRI==false)
              {
                    if(VerifierNbAreteIntersection(rI,RecPositifsVectors[j])){
                       PerimetreToral=PerimetreToral+ 2*(min(rI.getHauteur(),rI.getLargeur()));
                     }   
              }     
         }
         
     }   
 }
    
}

/**
   Enleve l'aire qui a ete soustrait en trop dans les intersectrions entre les aires negatives.

   @param RecIntersecVectorsSansDoublons le tableau d'intersection sans les doublons
   @param RecNegatifsVectors le tableau de rectangles negatifs.
   @param RecPositifsVectors le tableau de rectangles positifs.
   @param compteurIntersections compteur du nombre d'intersection.
   @param AireTotal Aire total de la forme.
   @param PerimetreToral Le perimetre total de la forme.
   @param compteuPositif compteur du nb de rec positifs
   @param compteurNegatif compteur du nb de rec negatifs.
*/
void EnleverAireCompteeEnTrops(vector<Rectangle> & RecIntersecVectors,vector<Rectangle> & RecNegatifsVectors,vector<Rectangle> & RecPositifsVectors,int& compteurIntersections,double &AireTotal ,int&compteuPositif,int & compteurNegatif){

 for (int i = 0; i < compteurIntersections; i++)
 {
   for (int j = i+1; j < compteurIntersections; j++)
   {
       if (equals(RecIntersecVectors[i],RecIntersecVectors[j])){
                AireTotal=AireTotal+RecIntersecVectors[i].aire(); 
                i++; 

       }else if(Intersection(RecIntersecVectors[i],RecIntersecVectors[j]))
       {
           Rectangle rI=RectangleIntersection(RecIntersecVectors[i],RecIntersecVectors[j]);
           AireTotal=AireTotal+ rI.aire(); 
       }
       
   }
    
 } 

 //Enlever l'aire communes des aires negatives compté de trop
 for (int i = 0; i < compteurNegatif; i++)
 {
    for (int j = i+1; j < compteurNegatif; j++)
    {
        if(Intersection(RecNegatifsVectors[i],RecNegatifsVectors[j])){
            Rectangle rI=RectangleIntersection(RecNegatifsVectors[i],RecNegatifsVectors[j]);
            //si cette intersection est dans une surface positive
            bool Inside=false;
            for (int z = 0; z < compteuPositif; z++)
            {
                if(Intersection(rI,RecPositifsVectors[z]))
                {
                    Inside=true;
                }
                
            }
            if(Inside==true){ 
            // on la retire
            AireTotal=AireTotal-rI.aire();
            }
        }
    }
    
 }
}

   /** 
    * Ceci est la methode principal qui calcule l'aire et le perimetre resultrant 
    * de l'union des rectangles positifs et negatifs.
    * 
    * @param args Unused. 
    * @return 0. 
    */

int main(int argc, const char** argv) {
    
    //Declaration des vectors contenant des rectangles (positif,negatif & rectangle issue d'intersections)
    vector<Rectangle> RectanglesVectors; 
    vector<Rectangle> RecPositifsVectors;
    vector<Rectangle> RecNegatifsVectors;
    vector<Rectangle> RecIntersecVectors;
    vector<Rectangle> RecIntersecVectorsSansDoublons;

    //Declaration des variables
    bool ValeursInvalide=false;
    int nbRectangle=0;
    int compteuPositif=0;
    int compteurNegatif=0;
    int compteurIntersections=0;
    double AireTotal=0;
    double PerimetreToral=0;
    double AireI=0;

    //Lecture et verification du fichier contenant les rectangles
    LectureFichier(RectanglesVectors,nbRectangle,ValeursInvalide);
    

    // Si les valeurs sont valides
    if(ValeursInvalide==false){
        
        //Les Rectangle positifs et Nedatifs sont placee separement dans deux Vectors.
        placerRectPositifsEtNegatifs(RectanglesVectors,RecPositifsVectors,RecNegatifsVectors,nbRectangle,compteuPositif,compteurNegatif);
        
        //partie positive
        CalculAireEtPerimetrePositif(RecPositifsVectors,AireTotal,PerimetreToral,compteuPositif);
        PlacerIntersectionDansTab(RecPositifsVectors,RecIntersecVectors,AireI,PerimetreToral,compteuPositif,compteurIntersections);
        CalculsAireEtPerimIntersection(RecIntersecVectors,AireI,PerimetreToral,AireTotal,compteurIntersections);
        
        //Partie negative
        SoustractionAireNegative(RecIntersecVectors,RecNegatifsVectors, RecPositifsVectors,AireTotal,compteurIntersections,compteuPositif, compteurNegatif);
        RectangleIntersectionSansDoublons( RecIntersecVectors,compteurIntersections,RecIntersecVectorsSansDoublons);
        CalculsPerimNegatif(RecIntersecVectorsSansDoublons,RecNegatifsVectors,RecPositifsVectors,compteurIntersections,PerimetreToral,compteuPositif,compteurNegatif);
        EnleverAireCompteeEnTrops(RecIntersecVectors,RecNegatifsVectors,RecPositifsVectors,compteurIntersections,AireTotal ,compteuPositif, compteurNegatif);
        
        //Affichage
        afficherResultats(AireTotal,PerimetreToral);

    }else{ 
     //Si les  valeurs sont invalides
     cout<<"ERREUR : Valeurs Invalides"<<endl;
    }
    return 0;

}
